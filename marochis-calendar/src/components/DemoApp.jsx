import React from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'

var testObject =
  [
    { title: 'Sprint #1', start: '2021-03-23', end: '2021-04-06', backgroundColor: 'red' },
    { title: 'Sprint #2', start: '2021-04-06', end: '2021-04-20', backgroundColor: 'green' },
    { title: 'Sprint #3', start: '2021-04-20', end: '2021-05-04', backgroundColor: 'blue' },
    { title: 'Previa da Sprint #3', start: '2021-04-27',end: '2021-04-28', backgroundColor: 'rgb(0,0,100)' }
  ];

localStorage.setItem('calendarData', JSON.stringify(testObject));
export default class DemoApp extends React.Component {
  render() {
    return (
      <FullCalendar
        plugins={[ interactionPlugin, dayGridPlugin ]}
        initialView="dayGridMonth"
        weekends={true}
        editable={true}
        selectable={true}
        selectMirror={true}
        dayMaxEvents={true}
        locale="pt-br"
        events={JSON.parse(localStorage.getItem('testObject'))}
        />
    )
  }
}
