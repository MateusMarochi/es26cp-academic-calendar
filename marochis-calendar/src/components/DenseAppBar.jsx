import React from "react";
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import SaveIcon from "@material-ui/icons/Save";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Chip from "@material-ui/core/Chip";
import Box from "@material-ui/core/Box";

import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";

import FullCalendar, { formatDate } from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import timeGridPlugin from "@fullcalendar/timegrid";

// localStorage.setItem("calendarData", JSON.stringify([]));

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}
const refHorarios = [
  ["07:30:00", "08:20:00", "09:10:00", "10:20:00", "11:10:00", "12:00:00"],
  ["08:20:00", "09:10:00", "10:00:00", "11:10:00", "12:00:00", "12:50:00"],
  ["13:00:00", "13:50:00", "14:40:00", "15:50:00", "16:40:00", "17:30:00"],
  ["13:50:00", "14:40:00", "15:30:00", "16:40:00", "17:30:00", "18:40:00"],
  ["18:40:00", "19:30:00", "20:20:00", "21:20:00", "22:10:00", "00:00:00"],
  ["19:30:00", "20:20:00", "21:10:00", "22:10:00", "23:00:00", "00:00:00"],
];

function startBy(value) {
  switch (value[1]) {
    case "M":
      return refHorarios[0][value[2]];
    case "T":
      return refHorarios[2][value[2]];
    case "N":
      return refHorarios[4][value[2]];
    default:
      return "00:00:00";
  }
}

function endBy(value) {
  switch (value[2]) {
    case "M":
      return refHorarios[1][value[3]];
    case "T":
      return refHorarios[3][value[3]];
    case "N":
      return refHorarios[5][value[3]];
    default:
      return "00:00:00";
  }
}
function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function pad(s, size) {
  s = String(s);
  while (s.length < (size || 2)) {
    s = "0" + s;
  }
  return s;
}
const drawerWidth = 240;

var data = new Date();
var dataAgora =
  data.getFullYear() +
  "-" +
  pad(data.getMonth() + 1, 2) +
  "-" +
  pad(data.getDate(), 2) +
  "T" +
  pad(data.getHours(), 2) +
  ":" +
  pad(data.getMinutes(), 2) +
  ":" +
  pad(data.getSeconds(), 2);

const paletaCores = [
  {
    value: "blue",
    label: "Azul",
  },
  {
    value: "green",
    label: "Verde",
  },
  {
    value: "red",
    label: "Vermelho",
  },
  {
    value: "yellow",
    label: "Amarelo",
  },
];
let todayStr = new Date().toISOString().replace(/T.*$/, "");
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "22ch",
      },
      flexGrow: 1,
    },
    fab: {
      position: "absolute",
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    appBar: {
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: "flex-end",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
    },
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    container: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
    visible: {
      visibility: "visible",
    },
    invisible: {
      visibility: "hidden",
    },
  })
);

export default function DenseAppBar() {
  const classes = useStyles();
  const theme = useTheme();
  const calendarRef = React.createRef();
  const disciplinasRef = React.createRef();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [openDisciplina, setOpenDisciplina] = React.useState(false);
  const [semana, setSemana] = React.useState("");
  const [periodo, setPeriodo] = React.useState("");
  const [horario, setHorario] = React.useState("");
  const [horarios, setHorarios] = React.useState([]);
  const [horarioCriado, setHorarioCriado] = React.useState(false);
  const [calendar, setCalendar] = React.useState(true);

  const [calendarData, setCalendarData] = React.useState(
    JSON.parse(localStorage.getItem("calendarData"))
  );

  if (calendarData == null || calendarData.length === 0) {
    setCalendarData([
      {
        title: "Sprint #1",
        start: "2021-03-23",
        end: "2021-04-06",
        backgroundColor: "red",
      },
      {
        title: "Sprint #2",
        start: "2021-04-06",
        end: "2021-04-20",
        backgroundColor: "green",
      },
      {
        title: "Sprint #3",
        start: "2021-04-20",
        end: "2021-05-04",
        backgroundColor: "blue",
      },
      {
        title: "Sprint #4",
        start: "2021-05-04",
        end: "2021-05-18",
        backgroundColor: "yellow",
      },
      {
        title: "Previa da Sprint #1",
        date: "2021-03-30",
        backgroundColor: "#640000",
      },
      {
        title: "Previa da Sprint #2",
        date: "2021-04-13",
        backgroundColor: "#146400",
      },
      {
        title: "Previa da Sprint #3",
        date: "2021-04-27",
        backgroundColor: "#070064",
      },
      {
        title: "Previa da Sprint #4",
        date: "2021-05-11",
        backgroundColor: "#645d00",
      },
    ]);
    localStorage.setItem("calendarData", JSON.stringify(calendarData));
  }
  const changeCalendar = (click) => {
    console.log(JSON.parse(localStorage.getItem("calendarData")));
    setCalendar(!calendar);
  };
  const semanaChange = (event) => {
    setSemana(event.target.value);
  };

  const periodoChange = (event) => {
    setPeriodo(event.target.value);
  };

  const horarioChange = (event) => {
    setHorario(event.target.value);
  };

  const criarHorario = () => {
    setHorarioCriado(true);
    if (semana !== "" && periodo !== "" && horario !== "") {
      let concat = semana + periodo + horario;
      if (horarios == null)
        setHorarios([
          {
            value: concat,
            color: currency,
            title: document.getElementById("nome-disciplina").value,
          },
        ]);
      else {
        let aux = horarios;
        horarios[horarios.length] = {
          value: concat,
          color: currency,
          title: document.getElementById("nome-disciplina").value,
        };
        setHorarios(aux);
      }
    }
  };
  const getEventos = () => {
    return calendarData;
  };

  const testReturn = () => {
    return "deu certo";
  };

  const criarEvento = () => {
    let calendarApi = calendarRef.current.getApi();
    calendarApi.addEvent({
      title: document.getElementById("nome-evento").value,
      start: document.getElementById("data-evento").value,
      end: document.getElementById("data-evento").value,
      backgroundColor: currency,
    });
  };

  const criarAula = () => {
    let calendarApi = calendarRef.current.getApi();
    // calendarApi.addEvent({
    //   title: document.getElementById("nome-evento").value,
    //   start: document.getElementById("data-evento").value,
    //   end: document.getElementById("data-evento").value,
    //   backgroundColor: currency,
    // });
  };

  const criarDisciplina = () => {
    // 15-06
    // 04-09
    console.log(horarios);
    let calendarApi = calendarRef.current.getApi();
    horarios.map((values) =>
      calendarApi.addEvent({
        title: "CD23CP",
        daysOfWeek: [values.value[0]],
        startRecur: "2021-06-15",
        endRecur: "2021-09-04",
        startTime: startBy(values.value),
        endTime: endBy(values.value),
        color: values.color,
      })
    );
    setHorarios([]);
    setSemana("");
    setPeriodo("");
    setHorario("");
    setOpen(false);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDrawerOpen = (info) => {
    setOpen(true);
    setAnchorEl(null);
  };
  const handleDisciplinaOpen = (info) => {
    setOpenDisciplina(true);
    setAnchorEl(null);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const handleDisciplinaClose = () => {
    setOpenDisciplina(false);
  };

  const handleEvento = (props) => {
    // console.log(props);
  };

  const [selectedDate, setSelectedDate] = React.useState(
    new Date("2014-08-18T21:11:54")
  );

  const [currentEvents, setCurrentEvents] = React.useState([]);

  const [currency, setCurrency] = React.useState("blue");

  const handleChange = (event) => {
    handleEvento();
    setCurrency(event.target.value);
  };

  const handleWeekendsToggle = () => {
    this.setState({
      weekendsVisible: !this.state.weekendsVisible,
    });
  };

  const handleDateSelect = (selectInfo) => {
    let title = prompt("Qual o nome do evento que será adicionado?");
    let calendarApi = selectInfo.view.calendar;

    calendarApi.unselect(); // clear date selection

    if (title) {
      calendarApi.addEvent({
        title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        backgroundColor: "#345234",
      });
    }
  };

  const handleEventClick = (clickInfo) => {
    let calendarApi = calendarRef.current.getApi();
    calendarApi.gotoDate("2021-05-24");
    let resposta = prompt("Para apagar digite 'A' e para editar digite 'E':");
    if (resposta != null) {
      if (resposta.toUpperCase() === "A") {
        if (
          prompt(
            "Para apagar digite 'S' para Sim ou 'N' para Não:"
          ).toUpperCase() === "S"
        )
          clickInfo.event.remove();
      }
      if (resposta.toUpperCase() === "E") {
        let nomeEditado = prompt("Digite o novo título do evento:");
        if (nomeEditado != null || nomeEditado !== "") {
          clickInfo.event = nomeEditado;
        }
        let dataEditada = prompt("Digite a nova data no formato DD-MM-AAAA:");
      }

      calendarApi.addEvent({
        title: document.getElementById("nome-evento").value,
        start: document.getElementById("data-evento").value,
        end: document.getElementById("data-evento").value,
        backgroundColor: currency,
      });
    }
  };

  const handleEvents = (events) => {
    // console.log(events);
    localStorage.setItem("calendarData", JSON.stringify(events));
    setCalendarData(JSON.stringify(events));
  };

  function renderEventContent(eventInfo) {
    return (
      <>
        <b>{eventInfo.timeText}</b>
        <i>{eventInfo.event.title}</i>
      </>
    );
  }

  const [valueBar, setValueBar] = React.useState(0);

  const handleChangeBar = (event, newValue) => {
    setValueBar(newValue);
  };

  function renderSidebarEvent(event) {
    return (
      <li key={event.id}>
        <b>
          {formatDate(event.start, {
            year: "numeric",
            month: "short",
            day: "numeric",
          })}
        </b>
        <i>{event.title}</i>
      </li>
    );
  }

  function createEventId() {
    return String("id001");
  }

  const disciplinaChange = (props) => {
    setHorarioCriado(false);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar variant="dense">
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={handleClick}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit">
            Marochi's Calendar
          </Typography>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleDrawerOpen}>Criar Evento</MenuItem>
            <MenuItem onClick={handleDisciplinaOpen}>
              Cadastrar Disciplina
            </MenuItem>
            <MenuItem onClick={handleClose}>Sair</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          Criar Evento
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <form className={theme.container}>
          <TextField
            required
            id="nome-evento"
            label="Nome do Evento"
            defaultValue=""
            variant="outlined"
            onChange={handleEvento}
          />
          <TextField
            id="data-evento"
            label="Data"
            type="date"
            defaultValue={todayStr}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={handleEvento}
          />
          <TextField
            id="cor-evento"
            select
            label="Cor do Evento"
            value={currency}
            onChange={handleChange}
            variant="outlined"
          >
            {paletaCores.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>

          <Fab
            color="primary"
            aria-label="add"
            className={theme.fab}
            onClick={criarEvento}
          >
            <SaveIcon />
          </Fab>
        </form>
      </Drawer>

      {/* Criar Disciplina */}
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={openDisciplina}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          Cadastrar Disciplina
          <IconButton onClick={handleDisciplinaClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <form className={theme.container}>
          <TextField
            required
            id="nome-disciplina"
            label="Nome da Disciplina"
            defaultValue=""
            variant="outlined"
            onChange={handleEvento}
          />
          <TextField
            id="cor-evento"
            select
            label="Cor do Evento"
            value={currency}
            onChange={handleChange}
            variant="outlined"
          >
            {paletaCores.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <Divider />
          {semana !== "" || periodo !== "" || horario !== "" ? (
            <Chip label={semana + periodo + horario} color="primary" />
          ) : (
            <Chip />
          )}
          {horarios.map((element) => (
            <Chip label={element.value} color="secundary" />
          ))}
          <FormControl component="fieldset">
            <FormLabel component="legend">Dia da semana</FormLabel>
            <RadioGroup
              aria-label="Dia da Semana"
              name="semana"
              value={semana}
              onChange={semanaChange}
              row
            >
              <FormControlLabel
                id="disciplina-semana-2"
                value="2"
                name="dia-semana"
                control={<Radio color="primary" />}
                label="2 - Segunda"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-semana-3"
                value="3"
                name="dia-semana"
                control={<Radio color="primary" />}
                label="3 - Terça"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-semana-4"
                value="4"
                name="dia-semana"
                control={<Radio color="primary" />}
                label="4 - Quarta"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-semana-5"
                value="5"
                name="dia-semana"
                control={<Radio color="primary" />}
                label="5 - Quinta"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-semana-6"
                value="6"
                name="dia-semana"
                control={<Radio color="primary" />}
                label="6 - Sexta"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-semana-7"
                value="7"
                name="dia-semana"
                control={<Radio color="primary" />}
                label="7 - Sábado"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
            </RadioGroup>
            <Divider />
            <FormLabel component="legend">Período do dia</FormLabel>
            <RadioGroup
              aria-label="Período"
              name="periodo"
              value={periodo}
              onChange={periodoChange}
              row
            >
              <FormControlLabel
                id="disciplina-periodo-M"
                value="M"
                control={<Radio color="primary" />}
                label="Manhã"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-periodo-T"
                value="T"
                control={<Radio color="primary" />}
                label="Tarde"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-periodo-N"
                value="N"
                control={<Radio color="primary" />}
                label="Noite"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
            </RadioGroup>
            <Divider />
            <FormLabel component="legend">Horário do período</FormLabel>
            <RadioGroup
              aria-label="Horário do Período"
              name="horario"
              value={horario}
              onChange={horarioChange}
              row
            >
              <FormControlLabel
                id="disciplina-horario-1"
                value="1"
                control={<Radio color="primary" />}
                label="1"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-horario-2"
                value="2"
                control={<Radio color="primary" />}
                label="2"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-horario-3"
                value="3"
                control={<Radio color="primary" />}
                label="3"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-horario-4"
                value="4"
                control={<Radio color="primary" />}
                label="4"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-horario-5"
                value="5"
                control={<Radio color="primary" />}
                label="5"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
              <FormControlLabel
                id="disciplina-horario-6"
                value="6"
                control={<Radio color="primary" />}
                label="6"
                labelPlacement="top"
                onChange={disciplinaChange}
              />
            </RadioGroup>
          </FormControl>
          <Fab
            color="primary"
            aria-label="add"
            className={theme.fab}
            onClick={criarHorario}
          >
            <AddIcon />
          </Fab>
          <Fab
            color="primary"
            aria-label="save"
            className={theme.fab}
            onClick={criarDisciplina}
          >
            <SaveIcon />
          </Fab>
        </form>
      </Drawer>

      {/*
          <div row>
            <Button value={1} variant="contained" onClick={changeCalendar}>
              Eventos
            </Button>
            <Button variant="contained" disabled>
              Disciplinas
            </Button>
          </div> */}
      <div className="container">
        <FullCalendar
          ref={calendarRef}
          plugins={[interactionPlugin, dayGridPlugin, timeGridPlugin]}
          headerToolbar={{
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay",
          }}
          buttonText={{
            today: "hoje",
            month: "mês",
            week: "semana",
            day: "dia",
            list: "lista",
          }}
          initialView="dayGridMonth"
          weekends={true}
          editable={true}
          selectable={true}
          nowIndicator={true}
          now={dataAgora}
          selectMirror={true}
          dayMaxEvents={true}
          locale="pt-br"
          initialEvents={calendarData}
          select={handleDateSelect}
          eventContent={renderEventContent} // custom render function
          eventClick={handleEventClick}
          eventsSet={handleEvents}
        />
      </div>
    </div>
  );
}
