import DenseAppBar from "./../components/DenseAppBar.jsx";

import "./../styles/global.css";

function App() {
  return <DenseAppBar />;
}

export default App;
